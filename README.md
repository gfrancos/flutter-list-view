# List View - CRUD API

A new Flutter project.

[<img src="https://user-images.githubusercontent.com/1295961/45949308-cbb2f680-bffb-11e8-8054-28c35ed6d132.png" align="center" width="850">](https://flutter.dev/)


## Getting Started

Here is how to get started with this project.
Make sure you have the latest flutter version, an Android/iOS Emulator installed.
( Follow the instructions from here: https://flutter.dev/docs/get-started/install )


**Clone the repo:**

```bash
git clone https://gitlab.com/gfrancos/flutter-list-view.git
```

**Install dependencies:**

Install the dependencies using this command (it's what `npm` is for node)

```bash
flutter pub get
```

**Start the app:**

You can either do this within VSCode or via CLI:

```bash
flutter run
```

