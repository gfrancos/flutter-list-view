import 'package:demo_data_table/model/user.dart';

final allUsers = <User>[
  User(firstName: 'Gabriel', lastName: 'Franco', salary: 15),
  User(firstName: 'Maria Jose', lastName: 'Chiriboga', salary: 30),
  User(firstName: 'Sarah', lastName: 'Winter', salary: 20),
  User(firstName: 'James', lastName: 'Summer', salary: 21),
  User(firstName: 'Lorita', lastName: 'Wilcher', salary: 18),
  User(firstName: 'Anton', lastName: 'Wilbur', salary: 32),
  User(firstName: 'Salina', lastName: 'Monsour', salary: 24),
  User(firstName: 'Sunday', lastName: 'Salem', salary: 42),
  User(firstName: 'Alva', lastName: 'Cowen', salary: 47),
  User(firstName: 'Jonah', lastName: 'Lintz', salary: 18),
  User(firstName: 'Kimberley', lastName: 'Kelson', salary: 33),
  User(firstName: 'Waldo', lastName: 'Cybart', salary: 19),
  User(firstName: 'Garret', lastName: 'Hoffmann', salary: 27),
  User(firstName: 'Margaret', lastName: 'Yarger', salary: 25),
  User(firstName: 'Foster', lastName: 'Lamp', salary: 53),
  User(firstName: 'Samuel', lastName: 'Testa', salary: 58),
  User(firstName: 'Sam', lastName: 'Schug', salary: 44),
  User(firstName: 'Alise', lastName: 'Bryden', salary: 41),
];
