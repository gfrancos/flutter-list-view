import 'dart:convert';

import 'package:demo_data_table/model/user.dart';
import 'package:demo_data_table/config/constants.dart';
import 'package:http/http.dart' as http;

class UserProvider {
  // Method to get Users in Database...
  static Future<List<User>?> downloadUsers() async {
    Uri uri =
        Uri(scheme: "https", host: ApiConstants.host, path: ApiConstants.path);

    var result = await http.get(uri);

    if (result.statusCode == 200) {
      final decodeData = json.decode(result.body);
      if (decodeData != null) {
        List<User> myModels;
        myModels = (json.decode(result.body) as List)
            .map((i) => User.fromJsonMap(i))
            .toList();
        return myModels;
      }
    }

    return null;
  }

  // Method to add an User in Database...
  static Future<bool> addUser(lastname, firstname, salary) async {
    final _queryParameters = {
      'LastName': lastname,
      'FirstName': firstname,
      'Salary': salary,
    };
    Uri uri = Uri(
      scheme: "https",
      host: ApiConstants.host,
      path: ApiConstants.path,
      queryParameters: _queryParameters,
    );

    var result = await http.post(uri);

    if (result.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Method to update an User in Database...
  static Future<bool> updateUser(id, firstname, lastname, salary) async {
    final _queryParameters = {
      'LastName': lastname,
      'FirstName': firstname,
      'Salary': salary,
    };

    Uri uri = Uri(
      scheme: "https",
      host: ApiConstants.host,
      path: ApiConstants.path + '/' + id.toString(),
      queryParameters: _queryParameters,
    );

    print('uri ' + uri.toString());

    var result = await http.put(uri);
    print('updateUser Response: ${result.body}');
    if (200 == result.statusCode) {
      return true;
    } else {
      return false;
    }
  }

  // Method to Delete an User from Database...
  static Future<bool> deleteUser(String userId) async {
    Uri uri = Uri(
      scheme: "https",
      host: ApiConstants.host,
      path: ApiConstants.path + '/' + userId.toString(),
    );

    final response = await http.delete(uri);
    if (200 == response.statusCode) {
      return true;
    } else {
      return false;
    }
  }
}
