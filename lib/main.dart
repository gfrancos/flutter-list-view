import 'package:demo_data_table/page/sortable_page.dart';
import 'package:demo_data_table/widget/tabbar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'Listado de Personas';

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primaryColor: Colors.deepPurple[200]),
        home: const MainPage(),
      );
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => TabBarWidget(
        title: MyApp.title,
        // ignore: prefer_const_literals_to_create_immutables
        tabs: [
          const Tab(icon: Icon(Icons.sort_by_alpha), text: 'ListView'),
          const Tab(icon: Icon(Icons.select_all), text: 'Seleccionar'),
          const Tab(icon: Icon(Icons.edit), text: 'Editar'),
        ],
        children: [
          const UserPage(),
          Container(),
          Container(),
        ],
      );
}
