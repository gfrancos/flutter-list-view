class User {
  int? id;
  String? firstName;
  String? lastName;
  int? salary;

  User({
    this.id,
    this.firstName,
    this.lastName,
    this.salary,
  });

  User copy({
    int? id,
    String? firstName,
    String? lastName,
    int? salary,
  }) =>
      User(
        id: id ?? this.id,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        salary: salary ?? this.salary,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          runtimeType == other.runtimeType &&
          firstName == other.firstName &&
          lastName == other.lastName &&
          salary == other.salary;

  @override
  int get hashCode => firstName.hashCode ^ lastName.hashCode ^ salary.hashCode;

  User.fromJsonMap(Map<String, dynamic> json) {
    id = json["ID"];
    lastName = json["LastName"]; //Id.fromJsonMap(json["_id"]);
    firstName = json["FirstName"];
    salary = json["Salary"];
  }

  Map<String, dynamic> toJson() => {
        "ID": id,
        "LastName": lastName,
        "FirstName": firstName,
        "Salary": salary,
      };

  User.fromMap(Map<String, dynamic> map) {
    id = map["ID"];
    lastName = map["LastName"];
    firstName = map["FirstName"];
    salary = map["Salary"];
  }

  User.map(dynamic obj) {
    id = obj["ID"];
    lastName = obj["LastName"];
    firstName = obj["FirstName"];
    salary = obj["Salary"];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["ID"] = id;
    map["LastName"] = lastName;
    map["FirstName"] = firstName;
    map["Salary"] = salary;

    return map;
  }
}
