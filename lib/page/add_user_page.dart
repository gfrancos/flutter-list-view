import 'package:flutter/material.dart';
import 'package:demo_data_table/provider/user_provider.dart';
import 'package:demo_data_table/main.dart';

class AddNewUserPage extends StatefulWidget {
  const AddNewUserPage({Key? key}) : super(key: key);

  @override
  _AddNewUserPageState createState() => _AddNewUserPageState();
}

class _AddNewUserPageState extends State<AddNewUserPage> {
  final _employeeFirstNameController = TextEditingController();
  final _employeeLastNameController = TextEditingController();
  final _employeeSalary = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Nuevo User'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _employeeFirstNameController,
                decoration: const InputDecoration(hintText: 'FirstName'),
              ),
              TextField(
                controller: _employeeLastNameController,
                decoration: const InputDecoration(hintText: 'Lastname'),
              ),
              TextField(
                controller: _employeeSalary,
                decoration: const InputDecoration(hintText: 'Salary'),
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                child: const Text(
                  'GUARDAR',
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.purple, // background
                  onPrimary: Colors.white, // foreground
                ),
                onPressed: () {
                  /* final body = {
                    "LastName": _employeeLastNameController.text,
                    "FirstName": _employeeFirstNameController.text,
                    "Salary": _employeeSalary.text,
                  };*/
                  UserProvider.addUser(
                          _employeeLastNameController.text,
                          _employeeFirstNameController.text,
                          _employeeSalary.text)
                      .then((success) {
                    if (success) {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: const Text('User ha sido agregado'),
                          actions: <Widget>[
                            ElevatedButton(
                              onPressed: () {
                                /* Navigator.pop(context);
                                _employeeLastNameController.text = '';
                                _employeeFirstNameController.text = '';
                                _employeeSalary.text = '';*/

                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => const MainPage(),
                                  ),
                                );
                              },
                              child: const Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    } else {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: const Text('Error Adding User!!!'),
                          actions: <Widget>[
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    }
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
