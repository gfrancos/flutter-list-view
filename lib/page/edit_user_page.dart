import 'package:flutter/material.dart';
import 'package:demo_data_table/provider/user_provider.dart';
import 'package:demo_data_table/main.dart';

class EditUserPage extends StatefulWidget {
  final int id;
  final String firstname;
  final String lastname;
  final int salary;

  const EditUserPage(
      {Key? key,
      required this.id,
      required this.firstname,
      required this.lastname,
      required this.salary})
      : super(key: key);

  @override
  _EditUserPageState createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  final _employeeFirstNameController = TextEditingController();
  final _employeeLastNameController = TextEditingController();
  final _employeeSalary = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //Inicializa los campos de texto con la data
    _employeeFirstNameController.text = widget.firstname;
    _employeeLastNameController.text = widget.lastname;
    _employeeSalary.text = widget.salary.toString();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Editar User'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _employeeFirstNameController,
                decoration: const InputDecoration(hintText: 'FirstName'),
              ),
              TextField(
                controller: _employeeLastNameController,
                decoration: const InputDecoration(hintText: 'Lastname'),
              ),
              TextField(
                controller: _employeeSalary,
                decoration: const InputDecoration(hintText: 'Salary'),
                keyboardType: TextInputType.number,
              ),
              ElevatedButton(
                child: const Text(
                  'GUARDAR',
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.purple, // background
                  onPrimary: Colors.white, // foreground
                ),
                onPressed: () {
                  /* final body = {
                    "LastName": _employeeLastNameController.text,
                    "FirstName": _employeeFirstNameController.text,
                    "Salary": _employeeSalary.text,
                  };*/
                  UserProvider.updateUser(
                          widget.id.toString(),
                          _employeeFirstNameController.text,
                          _employeeLastNameController.text,
                          _employeeSalary.text)
                      .then((success) {
                    if (success) {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: const Text('User ha sido modificado'),
                          actions: <Widget>[
                            ElevatedButton(
                              onPressed: () {
                                /* Navigator.pop(context);
                                _employeeLastNameController.text = '';
                                _employeeFirstNameController.text = '';
                                _employeeSalary.text = '';*/

                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => const MainPage(),
                                  ),
                                );
                              },
                              child: const Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    } else {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: const Text('Error Editing User!!!'),
                          actions: <Widget>[
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    }
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
