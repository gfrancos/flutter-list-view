import 'package:demo_data_table/provider/user_provider.dart';
import 'package:demo_data_table/page/add_user_page.dart';
import 'package:demo_data_table/page/edit_user_page.dart';
import 'package:flutter/material.dart';
import 'package:demo_data_table/main.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: UserPage(),
    );
  }
}

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: UserProvider.downloadUsers(),
        builder: (context, AsyncSnapshot snapshot) {
          final users = snapshot.data;
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.separated(
              separatorBuilder: (context, index) {
                return const Divider(
                  height: 2,
                  color: Colors.black,
                );
              },
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(
                      users[index].firstName + " " + users[index].lastName),
                  subtitle: Text('Salary: ${users[index].salary}'),
                  // <Add>
                  trailing: PopupMenuButton(
                    itemBuilder: (context) {
                      return [
                        const PopupMenuItem(
                          value: 'edit',
                          child: Text('Editar'),
                        ),
                        const PopupMenuItem(
                          value: 'delete',
                          child: Text('Borrar'),
                        )
                      ];
                    },
                    onSelected: (String value) {
                      if (value == 'edit') {
                        print('Vamos a editar ' + users[index].id.toString());
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditUserPage(
                                id: users[index].id,
                                firstname: users[index].firstName,
                                lastname: users[index].lastName,
                                salary: users[index].salary),
                          ),
                        );
                      } else {
                        showAlertDialog(context, users[index].id.toString(),
                            users[index].firstName, users[index].lastName);
                      }
                    },
                  ),
                );
              },
              itemCount: users.length,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const AddNewUserPage(),
            ),
          );
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

showAlertDialog(
    BuildContext context, String id, String firstname, String lastname) {
  // set up the buttons
  Widget cancelButton = ElevatedButton(
    child: const Text("Cancelar"),
    onPressed: () {
      Navigator.pop(context);
      /*Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const MainPage(),
        ),
      );*/
    },
  );
  Widget continueButton = ElevatedButton(
    child: const Text("Continuar"),
    onPressed: () {
      UserProvider.deleteUser(id);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const MainPage(),
        ),
      );
    },
  );
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text("Mensaje"),
    content:
        Text("Desea eliminar el usuario " + firstname + " " + lastname + "?"),
    actions: [
      cancelButton,
      continueButton,
    ],
  );
  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
